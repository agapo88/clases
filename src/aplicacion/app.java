package aplicacion;

import clases.Informatica;
import clases.Lenguaje;
import clases.Persona;
import clases.Programador;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Created by USUARIO on 5/07/2017.
 */
public class app {

    public static void main(String[] args) {
        Informatica personal = null;

        Scanner sn = new Scanner(System.in);
        boolean salir = false;
        int opcion;

        while (!salir) {

            System.out.println("1. Ingresar cantidad de Personal");
            System.out.println("2. Ingresar Personal");
            System.out.println("3. Listar Programadores");
            System.out.println("4. Listar Técnicos de Soporte");
            System.out.println("5. Ingresar Lenguajes de programación");
            System.out.println("5. Listar Lenguajes de programación");
            System.out.println("5. Salir");

            System.out.println( "SISTEMA DE REGISTRO DE PERSONAL DE INFORMATICA" );

            try {
                System.out.println("Seleccione una de las opciones");
                opcion = sn.nextInt();

                switch (opcion) {
                    case 1:
                        System.out.println("Cuantos personas desea ingresar?");
                        personal = new Informatica(sn.nextInt());
                        break;

                    case 2:
                        if( personal == null ){
                            System.out.println( "Aun no ha ingresado la cantidad de personas que desea ingresar, verifique. \n" );
                        }else{
                            System.out.println( "Es programador: ( S/N )" );

                            if( sn.next().equalsIgnoreCase("S") )
                                personal.setEsProgramador( true );

                            Persona persona = new Persona();
                            System.out.println("Ingrese Nombre:");
                            persona.setNombres( sn.next() );
                            System.out.println("Ingrese Apellidos:");
                            persona.setApellidos( sn.next() );
                            System.out.println( "Seleccione su genero: ( M/F )" );
                            if( sn.next().equalsIgnoreCase("M") )
                                persona.setGenero( true );

                            System.out.println( "Ingrese su edad" );
                            persona.setEdad( sn.nextInt() );
                            System.out.println( "Ingrese su nacionalidad" );
                            persona.setNacionalidad( sn.next() );
                            System.out.println( "Ingrese sus años de experiencia" );
                            persona.setExperiencia( sn.nextInt() );

                            // AGREGAR PROGRAMADOR
                            if( personal.getEsProgramador() )
                            {
                                System.out.println( "Cuantos Lenguajes conoce" );
                                Programador programador = new Programador( sn.nextInt() );
                                System.out.println( "Lenguajes>>> " + programador.getCantidadReal() );
                                for ( int i=0; i < programador.getCantidadReal(); i++ ){
                                    Lenguaje lenguaje = new Lenguaje();
                                    System.out.println("Ingrese Lenguaje:");
                                    lenguaje.setLenguaje( sn.next() );
                                    System.out.println("Es Lenguaje Compilado: (S/N)");
                                    if( sn.next().equalsIgnoreCase("S") )
                                        lenguaje.setEsCompilado( true );

                                    programador.adicionarLenguaje( lenguaje );
                                }

                                programador.adicionarProgramador( persona );
                                personal.adicionarProgramador( programador );
                            }
                            // AGREGAR TECNICO DE SOPORTE

                        }
                        break;

                    case 3:
                        System.out.println(">>>>> LISTA DE PROGRAMADORES <<<<<");
                        if( personal == null ){
                            System.out.println( "Aun no ha ingresado la cantidad de personal que desea ingresar, verifique. \n" );
                        }else{
                            personal.mostrarListadoProgramadores();
                        }
                        break;

                    case 4:
                        System.out.println(">>>>> LISTA DE TECNICOS EN SOPORTE <<<<<");
                        if( personal == null ){
                            System.out.println( "Aun no ha ingresado la cantidad de personal que desea ingresar, verifique. \n" );
                        }else{
                            personal.mostrarListadoTecnicosSoporte();
                        }
                        break;

                    case 5:
                        salir = true;
                        break;
                    default:
                        System.out.println("Solo números entre 1 y 5");
                }
            } catch (InputMismatchException e) {
                System.out.println("Debes insertar un número");
                sn.next();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }
}
