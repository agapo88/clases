package clases;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by USUARIO on 6/07/2017.
 */
public class Informatica {
    private Persona[] lstPersonal;
    private List<Persona> lstTecnicosSoporte;
    private List<Programador> lstProgramadores;
    private boolean esProgramador;

    public Informatica(int cantidadPersonal ) {
        lstPersonal = new Persona[ cantidadPersonal ];
        lstTecnicosSoporte = new ArrayList<>();
        lstProgramadores = new ArrayList<>();
    }

    public boolean getEsProgramador() {
        return esProgramador;
    }

    public void setEsProgramador(boolean esProgramador) {
        this.esProgramador = esProgramador;
    }

    public Informatica(Persona[] lstPersonal) {
        this.lstPersonal = lstPersonal;
    }

    public List<Programador> getListadoProgramadores() {
        return lstProgramadores;
    }

    public List<Persona> getListadoTecnicos() {
        return lstTecnicosSoporte;
    }


    public void mostrarListadoProgramadores(){
        if( lstProgramadores.size() > 0 ){
            for (Programador p: lstProgramadores
                    ) {
                        p.mostrarListadoProgramadores();
            }
        }else{
            System.out.println( "No ha ingresado ningún Programador, verifique. \n" );
        }
    }

    public void mostrarListadoTecnicosSoporte(){
        if( lstTecnicosSoporte.size() > 0 ){
            for (Persona p: lstTecnicosSoporte
                    ) {
                System.out.println(
                        "- NOMBRES: " + p.getNombres() + " APELLIDOS: " + p.getApellidos() + " EDAD: " + p.getEdad()
                                + " GENERO: " + ( p.getGenero() ? 'M' : 'F' + " Años de Experiencia: " + p.getExperiencia() )
                );
            }
        }else{
            System.out.println( "No ha ingresado ningún Técnico en Soporte, verifique. \n" );
        }
    }


    public void adicionarTecnicoSoporte( Persona p ){
        lstTecnicosSoporte.add( p );
    }

    public void adicionarProgramador( Programador p ){
        lstProgramadores.add( p );
    }

}
