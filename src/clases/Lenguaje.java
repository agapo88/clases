package clases;

/**
 * Created by USUARIO on 5/07/2017.
 */
public class Lenguaje {
    private String lenguaje;
    private Boolean esCompilado;

    public String getLenguaje() {
        return lenguaje;
    }

    public void setLenguaje(String lenguaje) {
        this.lenguaje = lenguaje;
    }

    public Boolean getEsCompilado() {
        return esCompilado;
    }

    public void setEsCompilado(Boolean esCompilado) {
        this.esCompilado = esCompilado;
    }
}
